import bson

class Learning(object):
	"""docstring for Selector"""
	def __init__(self):
		super(Selector, self).__init__()
	def compare(self,tag_relation,example1,example2):
		d={}#k -> ch
		d2={}#k ->k2
		d3={}#ch -> ch2
		name1,ext1=os.path.splitext(os.path.basename(example1))
		name2,ext2=os.path.splitext(os.path.basename(example12))
		c=0
		for elem in os.listdir("models/"):
			a=int(elem.split(".")[0])
			if c<a:
				c=a
		with open(example1) as f:
			with open(example2) as f2:
				r2=f2.read()
				for k,ch in enumerate(f.read()):
					d3[k]=ch
					d2[k]=K2
					d3[ch]=r2[k]
					with open("models/"+str(c+1)+".compare_"+tag_relation+"_"+str(name1)+"_"+str(name2)+"_1") as f2:
						f2.write(bson.dumps(d))
					with open("models/"+str(c+1)+".compare_"+tag_relation+"_"+str(name1)+"_"+str(name2)+"_2") as f2:
						f2.write(bson.dumps(d2))
					with open("models/"+str(c+1)+".compare_"+tag_relation+"_"+str(name1)+"_"+str(name2)+"_3") as f2:
						f2.write(bson.dumps(d3))
	def conclusion(self,tag_relation):
		"""
		dado una cantidad de modelos considerable, se buscara de optimizar funsionando todos estos
		en un modelo que pueda tener la menor cantidad e errores partiendo de estos modelos

		esto se usar con algoritmos geneticos ya que quizas lo mejor seria tener varios modelos de donde elegir
		"""
	def predict(self,tag_relation,var_independent,value,var_dependent):
		"""
		Este intenta buscar entre modelos que tengan la misma etiqueta de relacion
		un punto el cual conincida o este en medio de dos puntos del modelo 
		en el eje x(independiente) con esto crear una recta entre los dos puntos
		y cortar en "y" en la posicion del x para obtener la prediccion, se debe tener
		en cuenta la distancia del punto a la recta ya que se buscara el resultado que tenga
		el modelo mas cercano 

		tambien de no estar lo suficiente mente cerca, exede el limite, se considerar la creacion
		de un nuevo modelo, se lanzara la prediccion de ser afirmativa, se guarda en el nuevo modelo
		y de caso contrario se toma nota para que sea el resultado del modelo
		"""
	def vars_dependencies(self,varname,*dependencies):
		"""
		se trata de indicar que una variable depende de otras 
		por ejemplo la posicion: depende de x, y,z 
		que a su vez son representaciones de algun tipo de modelo
		
		nota: las variables mas basicas nacen a partir del modelo de datos 
		es decir la lectura de datos al hacer un open y un read
		donde "indice" y "valor" seran las primitivas para crear por ejemplo 
		la variable "x" dependencia de la variable posicion 

		"valor" e "indice" son primitivas y no variables asi que no hay que confundirse
		por defecto el nombre de la varible sera devuelto como el valor que tiene esta en
		el modelo

		el "indice" que la variable tendra en el modelo que se este evaluando actualmente se le
		llamara "id" para diferenciarlo de la primitiva "indice"
		"""
	def var_can_know(self,var_independent,value,var_dependent):
		"""
		determina si puede llegar a un resultado de una variable sabiendo sus dependencias
		o llegar al valor de una variable dependiente dando el valor de otra variable
		si conocer un modelo previo.

		Esto podria ser util para generar modelos mas directos y no tener que pasar por todos
		sus ancestros
		"""

	def var_relation(self,varname,model,relation):
		"""
		retorna una variable a la cual se esta haciendo referencia explicitamente a un modelo
		esto ya que en un contexto pueden existir variables con iguales nombre y podria ser necesario
		saber a cual modelo esta haciendo referencia
		"""
	def create_figure(self,figure,example1,example2):
		"""
		se trata de crear un modelo de representacion de una variable 
		es decir contextualizar un attributo en el modelo, algo parecido a memorize
		"""
		#posicion







	def memorize(self,tag_relation,folder):
		import os
		for n_frame,elem in enumerate(os.listdir(folder)):
			ext=os.path.splitext(elem)[1]
			if ext==".jpg" 
				or ext==".png"
				or ext==".jpeg"
				or ext==".bmp":
				pass
			elif ext==".mp4" 
				or ext==".ogv"
				or ext==".wmp":
				pass
			elif ext==".wav" 
				or ext==".mp3"
				or ext==".ogg":
				pass
			#-------------------------
			#seccion de datasets
			elif ext==".json":
				pass 
			#-------------------------

			else:#por defecto son archivos de texto
				d={}
				with open(folder+"/"+elem) as f:
					for k,ch in enumerate(f.read()):
						d[k]=ord(ch)	
					with open("models/memorize_"+tag_relation+"_"+str(n_frame)) as f2:
						f2.write(bson.dumps(d))






		
		